<?php 
require "shop_pdo.php";
$prekiumasyvas = paimti_dbaze();
$preke = null;
if (isset($_GET['redaguoti'])) {
    foreach ($prekiumasyvas as $prekes) {
        if ($_GET['redaguoti'] == $prekes->ID) {
            $preke = $prekes;
            break;
        }
    }
}
if (isset($_GET['preke']) && isset($_GET['kaina']) && $preke != null) {
    redaguoti_db($preke->ID, trim($_GET['preke']), trim ($_GET['kaina']));
    header("Location: ./administracinis_sarasas.php");
}
?>

<?php if (isset($_GET['redaguoti'])): $redirect=false; ?>
<form>
    <input  type='hidden' 
            value='<?php echo $_GET['redaguoti']; ?>' 
            name='redaguoti' />
    <input  type='text' 
            value='<?php echo $preke->preke; ?>'
            name='preke'/>
    <input  type='number' step="0.01"
            value='<?php echo $preke->kaina; ?>'
            name='kaina'/>


    <input  type='submit' />
</form>
<?php endif; 


?>